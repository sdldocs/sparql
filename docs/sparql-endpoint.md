# SPARQL 엔드포인트란 무엇이며 왜 중요한가?<sup>[1](#footnote_1)</sup>

데이터로 작업할 때 데이터 흐름의 중요한 문제, 즉 데이터 생성에서 저장, 수정과 용도 변경에 이르는 데이터의 이동은 역사적으로 다음과 같은 여러 가지 불일치로 인해 문제를 일으켜 왔다.

- 데이터 항목(즉, 주체 또는 엔티티) 식별자
- 데이터 액세스 프로토콜
- 데이터 구조
- 데이터 위치
- 데이터 쿼리 언어

![](./images/sparql-endpoint/1_OSlXNQD2D8bRX6D0piGEgg.webp)

## SPARQL Query Language란?
[SPARQL Query Language]()는 [RDF 언어]() 문장/문장의 모음으로 표현된 데이터에 대해 데이터 조작과 데이터 정의 작업을 수행하기 위한 선언적(declarative) 쿼리 언어(SQL과 같은)이다.

모든 SPARQL 쿼리에는 **Solution Modifier**(또는 head)와 **Query Body**가 있다. **Solution Modifier**는 다양한 타입의 SPARQL 쿼리 솔루션을 분류하기 위한 기초를 제공한다. **Query Body**는 쿼리 범위가 적용되는 엔티티 관계를 나타내는 RDF 문 패턴([Turtle 표기법]() 을 사용하여 표현됨) 모음으로 구성된다.

### 읽기 지향 쿼리 타입

- **`SELECT`** - Solution Modifier는 쿼리 솔루션을 표 형식으로 투영하는 SQL `SELECT` 리스트와 매우 유사한 `SELECT` 리스트의 형태를 보인다.

```sparql
# SELECT
#   {TABULAR-OUTPUT-SELECTION}
# WHERE
#   {Entity Relationship Types represented in
#    RDF-Turtle, supporting use of variables,
#    constants, and blank nodes as identifier 
#    types}
#
# Example:
#   Present a Query Solution in Tabular form 
#   that lists sentence subjects, predicates, 
#   and objects identified by the variables 
#   ?s, ?p, and ?o, and limit row count to 100
SELECT DISTINCT 
       ?s ?p ?o
WHERE 
   { ?s ?p ?o }
LIMIT 100
```

- **`CONSTRUCT`** - Solution Modifier는 RDF 문장/문 그래프 사양의 형태를 취한다.

```sparql
# CONSTRUCT
#   {ENTITY-RELATIONSHIP-GRAPH-OUTPUT-SELECTION}
# WHERE
#   {Entity Relationship Types represented 
#    in RDF-Turtle that includes support for 
#    variables, constants, and blank nodes 
#    as identifier types}
#
# Example:
#   Present a Query Solution in Entity Relationship 
#   Graph form comprising sentence subjects, 
#   predicates, and objects identified by the 
#   variables ?s, ?p, and ?o, and limit 
#   relationship count to 100
CONSTRUCT 
    { ?s ?p ?o }
WHERE
    { ?s ?p ?o }
LIMIT 100
```

- **`DESCRIBE`** - Solution Modifier가 선택한 엔티티를 설명하는 RDF 문장/문 그래프 사양의 형태를 취하는 `CONSTRUCT`의 변형이다.

```sparql
# DESCRIBE
#   {ENTITY-RELATIONSHIP-GRAPH-OUTPUT-SELECTION}
# WHERE
#   {Entity Relationship Types represented in
#    RDF-Turtle, supporting use of variables,
#    constants, and blank nodes as identifier 
#    types}
#
# Example:
#   Present a Query Solution in Entity Relationship 
#   Graph form that describes the all subjects of 
#   sentences where the subjects, predicates, and 
#   objects are identified by the variables ?s, ?p, 
#   and ?o, and limit relationship count to 100
DESCRIBE ?s 
WHERE
   { ?s ?p ?o }
LIMIT 100
```

- **`ASK`** - 이 쿼리 타입은 간단한 boolean(`Yes` 또는 `No`) 쿼리 솔루션을 반환한다.

```sparql
# ASK 
# WHERE
#   {Entity Relationship Types represented in
#    RDF-Turtle, supporting use of variables,
#    constants, and blank nodes as identifier 
#    types}
#
# Example:
#   Inquire about the existence of sentences that 
#   have the subjects, predicates, and objects 
#   identified by variables ?s, ?p, and ?o
ASK
WHERE
  { ?s ?p ?o }
```

### 쓰기 지향 쿼리 타입
쓰기 지향 SPARQL 쿼리는 **명명된 그래프(Named Graph)**라고 하는 특정 DBMS 또는 트리플/쿼드 저장소 문서(또는 데이터 소스)와 연관된 RDF 문장/문장 모음에 대해 `CREATE`, `INSERT`, `UPDATE`, `DELETE`와 기타 "변경" 작업을 수행한다.

- **`CREATE`** - 새로운 빈 명명된 그래프를 생성

```sparql
# SILENT keyword ensures this operation is always 
# successful, even if the target Named Graph wasn't 
# explicitly created by a previous CREATE statement
CREATE SILENT GRAPH <urn:my:document:1>
```

- **`INSERT`** - 명시적으로 또는 SPARQL 쿼리 본문에서 충족된 조건에 따라 명명된 그래프에 RDF 문장을 추가한다.

```sparql
PREFIX : <#> 
INSERT
  { GRAPH <urn:my:document:1> 
    { :entity1 :relatedTo :entity2 . } 
  }
```

- **`COPY`** - 명명된 그래프 사이에 RDF 문장을 복사한다.

```sparql
COPY <urn:my:document:1> 
  TO <urn:my:document:2>
```

- **`ADD`** - 명명된 그래프에 RDF 문장을 추가한다.

```sparql
ADD <urn:my:document:2> 
 TO <urn:my:document:3>
```

- **`MOVE`** - 한 명명된 그래프에서 다른 명명된 그래프로 RDF 문장을 이동한다. 즉, 소스 명명된 그래프와 해당 문장은 대상 명명된 그래프에서 성공적으로 재생성된 후 영구적으로 제거된다.

```sparql
MOVE <urn:my:document:3> 
  TO <urn:my:document:4>
```

- **`DELETE`** - 대상 명명된 그래프에서 SPARQL 쿼리 본문의 조건을 만족하는 RDF 문장을 제거한다. 삭제 쿼리는 특정 문을 명시적으로 타깃팅할 수 있다.

```sparql
# Remove specific statements, where the sentence 
# subject, predicate, and objects are all identified 
# by constants in the SPARQL query
PREFIX : <#>
DELETE
  { GRAPH <urn:my:document:1> 
    { :entity1 :relatedTo :entity2 . } 
  }
```

또는 변수로 구축된 패턴을 만족하는 모든 문을 대상으로 한다.

```sparql
# Remove all attribute-names (predicates) and 
# attribute-values (objects) for a specific entity, 
# i.e., the RDF sentence subject is identified by 
# a constant while the predicate and object are 
# identified by variables
PREFIX : <#>
DELETE
  { GRAPH <urn:my:document:1> 
    { :entity1 ?p ?o . } 
  }
WHERE
  { GRAPH <urn:my:document:1> 
    { :entity1 ?p ?o . } 
  }
```

- **`CLEAR`** - 특정 명명된 그래프에서 모든 RDF 문장을 제거한다. 즉, 빈 명명된 그래프가 남게 된다.

```sparql
CLEAR GRAPH <urn:my:document:2>
```

- **`DROP`** - DBMS 또는 트리플/쿼드 스토어에서 명명된 그래프 전체를 제거한다.

```sparql
# SILENT keyword ensures this operation is always 
# successful, even if the target Named Graph wasn't 
# explicitly created by a previous CREATE statement
DROP SILENT GRAPH <urn:my:document:1>
```

### SPARQL Endpoint 이란?
**SPARQL Endpoint**는 **SPARQL 프로토콜** 요청을 수신하고 처리할 수 있는 HTTP 네트워크의 Point of Presence이다. 일반적으로 SPARQL Endpoint URL이라고 하는 URL로 식별뙨다.

### SPARQL Protocol 이란?
[**SPARQL 프로토콜**](https://www.w3.org/TR/sparql11-protocol/)은 SPARQL Endpoint를 통해 데이터에 대해 SPARQL 연산을 수행하기 위한 HTTP 기반 프로토콜이다. 수행되는 작업의 종류에 따라 HTTP 페이로드는 `GET`, `POST` 또는 `PATCH` 메서드를 사용하여 전송된다.

### Federated SPARQL 이란?
원래는**SPARQL-FED**라고 불렸고 지금도 그렇게 불리는 [**Federated SPARQL**](https://www.w3.org/TR/sparql11-federated-query/)은 원격 SPARQL Query Service endpoint에서 제공하는 RDF 문/문장(데이터)에 대한 액세스를 제공하는 federated 형태의 SPARQL 쿼리이다. 즉, SPARQL 프로토콜을 통해 SPARQL 쿼리가 페더레이션되는 방식이다.

SPARQL 쿼리 본문의 **`SERVICE`** 절은 쿼리 솔루션 생산 파이프라인의 일부로 원격 SPARQL endpoint와 해당 원격 endpoint에서 실행되는 실제 쿼리를 식별하는 데 사용된다.

```sparql
SELECT
  DISTINCT ?s ?p ?o
WHERE
  { 
    SERVICE <http://linkeddata.uriburner.com/sparql> 
      {
        SELECT ?s ?p ?o
        WHERE {?s ?o ?o}
        LIMIT 100                
      }
  }
```

### SPARQL 쿼리 직렬화 형식이란?
SPARQL 쿼리 결과 직렬화 형식은 SPARQL 쿼리 솔루션과 관련된 다양한 문서 콘텐츠 유형이다. 일부 쿼리 유형은 결과가 전달될 수 있는 가능한 직렬화 형식을 제한한다. 여기에는 다음이 포함되어 있다.

- **`text/html`** - `SELECT` 쿼리
- [application/sparql-results+xml](https://www.w3.org/TR/rdf-sparql-XMLres/) - `SELECT` 쿼리
- [application/sparql-results+json](https://www.w3.org/TR/sparql11-results-json/) - `SELECT` 쿼리
- **`text/turtle`** - `CONSTRUCT`와 `DESCRIBE` 쿼리 
- **`application/n-triples`** - `CONSTRUCT`와 `DESCRIBE` 쿼리
- **`text/plain`** - `CONSTRUCT`와 `DESCRIBE` 쿼리
- **`application/ld+json`** - `CONSTRUCT`와 `DESCRIBE` 쿼리
- **`application/rdf+xml`** - `CONSTRUCT`와 `DESCRIBE` 쿼리

### SPARQL 쿼리 서비스란?
SPARQL 쿼리 서비스는 RDF 문장 모음으로 표현된 데이터에 대해 선언적 데이터 정의와 데이터 조작 작업을 수행하기 위한 API를 제공하는 HTTP 서비스(웹 서비스라고도 함)이다. 이러한 종류의 서비스는 당연히 **데이터베이스 관리 시스템 애플리케이션(DBMS)** 또는 **Triple/Quad Store**가 HTTP 네트워크에서 해당 지점을 식별하는 URL, 즉 메시지(쿼리)가 발송되는 주소와 연관되어 제공된다.

## SPARQL 쿼리 서비스 사용

### HTML 기반 쿼리 편집기
대부분의 경우, SPARQL 엔드포인트는 쿼리 편집과 실행을 위한 간단한 인터페이스로 기능하도록 HTML 문서와 짝을 이루고 있다. 관례에 따라 이러한 종류의 문서의 URL에는 최종 경로 구성 요소 또는 웹 사이트의 정식 이름의 호스트 부분으로 리터럴 `sparql`이 포함된다.

**예들은 다음을 포함하고 있다**.

- `http://linkeddata.uriburner.com/sparql` - 무수한 데이터 소스와 API에서 "즉시" 연결된 데이터를 생성할 수 있는 URIBurner 서비스
- `http://lod.openlinksw.com/sparql` - 링크드 오픈 데이터 클라우드(또는 LOD 클라우드) 캐시
- `http://dbpedia.org/sparql` - DBpedia(LOD 클라우드의 핵심)
- `http://sparql.uniprot.org/` - Uniprot(Bioinformatics을 위한 최첨단 지식 그래프)
- `https://sparql.nextprot.org` - neXtProt(Bioinformatics을 위한 최첨단 지식 그래프)
- `https://sparql.rhea-db.org/sparql` - Rhea 반응 데이터(Bioinformatics을 위한 최첨단 지식 그래프)
- `https://sparql.orthodb.org` - OrthoDB 유전학 데이터(Bioinformatics을 위한 최첨단 지식 그래프)
- `https://sparql.omabrowser.org/sparql` - OMA("Orthologous MAtrix") 유전체학 데이터(Bioinformatics을 위한 최첨단 지식 그래프)

### 라이브 쿼리 예
쿼리는 HTTP 요청을 통해 엔드포인트로 전달된다. 엔드포인트와 연결된 서버는 문서 URL이 포함된 HTTP 응답을 통해 쿼리 솔루션을 반환한다.

```sparql
SELECT DISTINCT
  ?s3 AS ?appLabel 
  ?s7 AS ?versionLabel  
  ?s1 AS ?executableUri 
  ?s4 AS ?formatLabel
  ?s6 AS ?OSUri 
  ?s8 AS ?OSLabel 
  ?s2 AS ?downloadUrl 
FROM <urn:data:openlink:products> 
WHERE 
  {
    ?s1  a  ?s9 .
    ?s1  <http://schema.org/downloadUrl>  ?s2 .
    ?s1  <http://schema.org/name>  ?s3 .
    ?s1  <http://purl.org/dc/terms/format>  ?s4 .
    ?s1  <http://schema.org/name>  ?s5 .
    ?s1  <http://www.openlinksw.com/ontology/software#hasOperatingSystemFamily>  ?s6 .
    # FILTER ( ?s6 = <http://www.openlinksw.com/ontology/software#GenericLinux> ) .
    ?s1  <http://www.openlinksw.com/ontology/products#versionText>  ?s7 .
    ?s6  schema:name  ?s8 . 
       FILTER ( ?s9 IN ( <http://www.openlinksw.com/ontology/installers#ExecutableArchive> , 
                         <http://www.openlinksw.com/ontology/installers#InitializationFile>
              )       )
  }
ORDER BY 5 1
```

- 쿼리 형식

![](./images/sparql-endpoint/query_form_01.png)

- 라이브 결과

![](./images/sparql-endpoint/1_cxbZUaE_6KSacq_p6uu63w.webp)

### 운영체제 명령어
SPARQL 쿼리는 일반 HTTP 명령 호출 유틸리티인 `curl`을 통해 컴퓨터의 운영 체제에서 직접 실행할 수 있다.


**예**

다음 SPARQL 쿼리 텍스트를 로컬 파일(예: sample-query.sparql)에 복사한다.

```sparql
ELECT DISTINCT
  ?s3 AS ?appLabel 
  ?s7 AS ?versionLabel  
  ?s1 AS ?executableUri 
  ?s4 AS ?formatLabel
  ?s6 AS ?OSUri 
  ?s8 AS ?OSLabel 
  ?s2 AS ?downloadUrl
FROM <urn:data:openlink:products>
WHERE 
  {
    ?s1 a ?s9 .
    ?s1 <http://schema.org/downloadUrl> ?s2 .
    ?s1 <http://schema.org/name> ?s3 .
    ?s1 <http://purl.org/dc/terms/format> ?s4 .
    ?s1 <http://schema.org/name> ?s5 .
    ?s1 <http://www.openlinksw.com/ontology/software#hasOperatingSystemFamily> ?s6 .
## Uncomment (remove the leading hash "#" from) 
## the following line to filter by Operating 
## System Family; in this case, Generic Linux.
# FILTER ( ?s6 = <http://www.openlinksw.com/ontology/software#GenericLinux> ) .
    ?s1 <http://www.openlinksw.com/ontology/products#versionText> ?s7 .
    ?s6 schema:name ?s8 . 
  FILTER ( ?s9 in (<http://www.openlinksw.com/ontology/installers#ExecutableArchive>, <http://www.openlinksw.com/ontology/installers#InitializationFile> ) )
  }
## This ORDER BY clause will sort result rows 
## by the 6th and 1st columns (i.e., ?OSLabel 
## and ?appLabel)
ORDER BY 6 1
```

그런 다음 명령어를 사용하여 원하는 출력 직렬화 형식에 해당하는 명령을 실행한다.

- JSON으로 출력

```bash
QUERY=$(<sample-query.sparql) && curl -X POST -H "Accept:application/sparql-results+json" --data-urlencode "query=$QUERY" http://linkeddata.uriburner.com/sparql
```

- HTML로 출력

```bash
QUERY=$(<sample-query.sparql) && curl -X POST -H "Accept:text/html" --data-urlencode "query=$QUERY" http://linkeddata.uriburner.com/sparql
```

- CSV로 출력

```bash
QUERY=$(<sample-query.sparql) && curl -X POST -H "Accept:text/csv" --data-urlencode "query=$QUERY" http://linkeddata.uriburner.com/sparql
```

## SPARQL Endpoint의 장점
SPARQL 엔드포인트는 API를 통해 다음과 같은 고유한 이점을 제공한다.

- 선언적 데이터 상호 작용(조작과 정의)은 HTTP를 통해 통합될 수 있으며, 세분화된 RDF 문장/문장 모음으로 표현되는 데이터를 대상으로 한다.
- HTTP 문서 URL은 데이터 쿼리에 광범위한 유연성을 제공한다. 즉, URL의 각 구성 요소는 대상 엔드포인트에서 쿼리 솔루션의 특성까지 확장되는 쿼리의 세부 사항을 매개변수화하여 변경할 수 있는 슬롯이다.
- 쿼리 솔루션 문서로 HTML, JSON, CSV, RDF-Turtle, RDF-N-Triples, RDF-XML 등 다양한 콘텐츠 유형을 지원한다.
- 모든 쿼리 솔루션 문서의 콘텐츠 유형은 HTTP 콘텐츠 협상("con-neg")을 통해 협상할 수 있다.
- 엔드포인트는 모든 HTTP 호환 사용자 에이전트 또는 서비스로 액세스할 수 있다.

## 마치며
모든 SPARQL 엔드포인트에는 다양한 협상 가능한 문서 유형을 사용하여 쿼리 솔루션(결과 세트) 전달을 지원하는 `GET`, `POST`, `PATCH` 작업을 통해 세분화된 데이터 정의와 조작 작업을 제공하는 HTTP 프로토콜 확장(HTTP 자체를 제공한 것과 동일한 W3C)을 위한 액세스 포인트가 있다. 이미 웹 친화적인 API로 제공되는 이 강력한 솔루션이 제공하는 표현력과 플랫폼 독립성은 일부에 불과하며, 이들에 대한 독점적인 대안을 찾는 이유를 상상하기 어렵다!

하이퍼링크(특히 HTTP URI)를 통해 *Data is the New Electricity* 세상에서 우리는 데이터 정션박스 기능의 공급자로서 SPARQL 엔드포인트를 효과적으로 이용할 수 있을 것이다. 이 사실은 이미 방대하고 계속 성장하고 있는 링크드 오픈 데이터 클라우드(Linked Open Data Cloud)를 구성하는 노드와 관련된 SPARQL 엔드포인트의 수가 계속 증가하고 있는 것으로 알 수 있다.


![](./images/sparql-endpoint/1_qX77ujJcG1g94LoSjwgoUw.webp)

LOD 클라우드 그림에서 볼 수 있듯이, SPARQL 엔드포인트는 차세대 최신 애플리케이션, 서비스, 스마트 에이전트와 기기에 연료를 공급할 준비가 된 대규모 데이터 그리드의 일부이다. 전력망이 구축되고 작동하기 전까지는 전구, 토스터, 세탁기, 진공 청소기, 에어컨, 냉장고 등 가전제품을 위한 실행 가능한 시장은 존재하지 않았다.

![](./images/sparql-endpoint/1_uGNNbxwyfH1d5bYBfoZDgg.webp)

LOD Cloud의 필수 구성 요소인 Virtuoso를 사용하면 누구나 몇 개의 파일만 다운로드하면 SPARQL 엔드포인트(로컬 또는 연합 사용용)의 모든 기능과 정교함을 경험할 수 있다.

- Virtuoso Enterprise Edition - (1) 서버 바이너리, (2) 서버 구성 파일, (3) 라이선스 관리자 바이너리, (4) Windows, macOS 또는 Linux용 라이선스 파일.
- Virtuoso 오픈 소스 에디션 - (1) 서버 바이너리 및 (2) Windows, macOS 또는 Linux용 서버 구성 파일

<a name="footnote_1">1</a>: 이 페이지는 [What is a SPARQL Endpoint, and why is it important?](https://medium.com/virtuoso-blog/what-is-a-sparql-endpoint-and-why-is-it-important-b3c9e6a20a8b)을 편역한 것임.

### Related
- [What is a Virtuoso SPARQL Endpoint, and why is it important?](https://medium.com/virtuoso-blog/what-is-a-virtuoso-sparql-endpoint-and-why-is-it-important-5244df738a3e)
- [What is the Linked Open Data Cloud, and why is it important?](https://medium.com/virtuoso-blog/what-is-the-linked-open-data-cloud-and-why-is-it-important-1901a7cb7b1f)
- [Linked Open Data Cloud](http://lod-cloud.net/#subclouds) — 서브 클라우드로 분할
- [SPARQL Endpoint Monitor](http://sparqles.ai.wu.ac.at/) — Datahub.io가 제공하는 서비스
- [Tabulated Comparison of Triple/Quad Stores, Graph Databases, and Relational Database Management Systems (RDBMS)](https://tinyurl.com/y9tfd8av)
- [Virtuoso Enterprise Editions vs Open Source Editions Features Comparison Matrix](https://virtuoso.openlinksw.com/features-comparison-matrix/)
- [Virtuoso Server Binary Downloads via Google Spreadsheet](https://docs.google.com/spreadsheets/d/192wVlEVo56uKBk7qIsgUeEVmBNGSBJSH06lk-5J1If0/edit#gid=1359274047) — for Windows, macOS, and Linux
- [Virtuoso Server Binary Downloads via HTML-based Pivot Table](https://virtuoso.openlinksw.com/download-matrix-pivot-demo/) — for Windows, macOS, and Linux
-[Virtuoso Server Binary Downloads via basic HTML page](http://www.openlinksw.com/sparql/?default-graph-uri=&query=SELECT++DISTINCT+xsd%3Astring%28%3Fs3%29+as+%3FappLabel+%0D%0A%09%09%09%09+xsd%3Astring%28%3Fs7%29+as+%3FversionLabel%09+%0D%0A%09%09%09%09+%3Fs1+as+%3FexecutableUri+%0D%0A%09%09%09%09+xsd%3Astring%28%3Fs4%29+as+%3FformatLabel%0D%0A%09%09%09%09+%3Fs6+as+%3FOSUri+%0D%0A%09%09%09%09+xsd%3Astring%28%3Fs8%29+as+%3FOSLabel+%0D%0A%09%09%09%09+%3Fs2+as+%3FdownloadUrl++%0D%0AFROM+%3Curn%3Adata%3Aopenlink%3Aproducts%3E%0D%0AWHERE++%7B%0D%0A%09%09%09%3Fs1+a+%3Fs9+.%0D%0A%09%09%09%3Fs1+%3Chttp%3A%2F%2Fschema.org%2FdownloadUrl%3E+%3Fs2+.%0D%0A%09%09%09optional+%7B%3Fs1+%3Chttp%3A%2F%2Fschema.org%2Fname%3E+%3Fs3+.%7D%0D%0A%09%09%09%3Fs1+%3Chttp%3A%2F%2Fpurl.org%2Fdc%2Fterms%2Fformat%3E+%3Fs4+.%0D%0A%09%09%09optional+%7B%3Fs1+%3Chttp%3A%2F%2Fschema.org%2Fname%3E+%3Fs5%7D+.%0D%0A%09%09%09%3Fs1+%3Chttp%3A%2F%2Fwww.openlinksw.com%2Fontology%2Fsoftware%23hasOperatingSystemFamily%3E+%3Fs6+.%0D%0A%09%09%09%23+FILTER+%28%3Fs6+%3D+%3Chttp%3A%2F%2Fwww.openlinksw.com%2Fontology%2Fsoftware%23GenericLinux%3E%29+.%0D%0A%09%09%09%3Fs1+%3Chttp%3A%2F%2Fwww.openlinksw.com%2Fontology%2Fproducts%23versionText%3E+%3Fs7+.%0D%0A%09%09%09optional+%7B%3Fs6+schema%3Aname+%3Fs8+.+%7D%0D%0A++++++++++++++++++++++++filter+%28%3Fs9+in+%28%3Chttp%3A%2F%2Fwww.openlinksw.com%2Fontology%2Finstallers%23ExecutableArchive%3E%2C+%3Chttp%3A%2F%2Fwww.openlinksw.com%2Fontology%2Finstallers%23InitializationFile%3E%29+%29%0D%0A%09%09%7D%0D%0AORDER+BY+5+1%0D%0A&should-sponge=&format=text%2Fhtml&timeout=30000000) — for Windows, macOS, and Linux
- [Virtuoso Download & License Generation Service](https://shop.openlinksw.com/license_generator/virtuoso/) — free evaluation licenses and installer archives for Windows, macOS, and Linux
