# 데이터 필터링
[이전 페이지](data-grouping.md)에서 Game of Thrones 데이터에 있는 인물 나이의 최대값과 같은 데이터세트에 대한 특정 정보를 찾는 방법에 대해 다루었다. 이제 SPARQL의 FILTER 기능을 사용하여 필요한 데이터를 찾기 위해 데이터세트를 필터링하는 방법을 알아보자.

## 학습할 키워드

- FILTER - 유용한 결과를 반환하기 위한 필터링 쿼리 
- SPARQL이 제공하는 다양한 필터링 기능
- SPARQL이 날짜 및 시간을 처리하는 방법

## 필터
어떻게 데이터세트에 있는 특정 연령 이상의 모든 인물을 찾을 수 있을까? SPARQL의 FILTER 함수를 사용하여 이를 수행할 수 있다. 이 쿼리는 10세 이상의 데이터세트에 있는 모든 인물의 이름과 성을 출력한다.

```
PREFIX GOT: <https://tutorial.linked.data.world/d/sparqltutorial/>

SELECT ?FName ?LName ?Age

WHERE {
	?person GOT:col-got-fname ?FName .
    ?person GOT:col-got-lname ?LName .
    ?person GOT:col-got-age ?Age .
	FILTER (?Age > 10)
}
```

결과는 아래와 같다.

```
FName       LName       Age
----------------------------
Tyrion      Lannister     27
Jaime       Lannister     34
Daenerys    Targaryen     16
Cersei      Lannister     34
Jon         Snow          17
Sansa       Stark         14
Arya        Stark         11
```

필터 기능을 사용하면 원하는 결과를 위하여 필터링할 수 있습니다. 이 예에서는 필터링할 변수를 선택하고 SPARQL의 필터 함수 중 하나를 적용했다. 

```
FILTER (?Age > 10)
```

위 필터가 10보다 많은 나이의 인물을 필터링하는 것과 마찬가지로 10보다 적거나 같은 나이의 인물을 필터링할 수도 있다. [SPARQL의 필터 함수 목록("<", "=", "<=" 등)](https://docs.data.world/tutorials/sparql/list-of-sparql-filter-functions.html)을 참고한다.


## 필터링된 결과 정렬
이전 결과는 연령별 순서가 아니라 임의의 순서로 나열된다. 이미 결과를 정렬할 줄 알고 있으니, 가장 나이 역순으로 결과를 정렬할 때 필요한 쿼리를 작성한다.

```
PREFIX GOT: <https://tutorial.linked.data.world/d/sparqltutorial/>

SELECT ?FName ?LName ?Age

WHERE {
	?person GOT:col-got-fname ?FName .
    ?person GOT:col-got-lname ?LName .
    ?person GOT:col-got-age ?Age .
	FILTER (?Age > 10)
}
ORDER BY DESC(?Age)
```

결과는 아래와 같다.

```
FName       LName       Age
----------------------------
Cersei      Lannister     34
Jaime       Lannister     34
Tyrion      Lannister     27
Jon         Snow          17
Daenerys    Targaryen     16
Sansa       Stark         14
Arya        Stark         11
```

이제 데이터세트의 10살 이상의 인물을 나이 역순으로 정렬한 목록을 출력하였다. 

## 날짜 필터링
이제 조금 다른 것을 시도해 보려고 한다. 270년 이전에 태어난 데이터세트의 모든 인물 이름과 성을 반환하려고 한다.

데이터 세트 "GOT" 내에 "생일"이라는 열이 있다는 것을 알고 있을 수도 있다. 이 열에는 표준 형식(yyyy-mm-dd)으로 모든 인물의 (대략적인) 생년월일을 갖고 있다. 일반적으로 SPARQL 프로세서는 이 날짜들을 자동으로 읽으며, 우리가 정수(나이)와 같은 방식으로 필터링할 수 있는 기능을 제공한다. 실제로 다음과 같이 동작한다.

```
PREFIX GOT: <https://tutorial.linked.data.world/d/sparqltutorial/>
PREFIX xsd:<http://www.w3.org/2001/XMLSchema#>

SELECT ?FName ?LName ?BirthDate

WHERE {
	?person GOT:col-got-fname ?FName .
    ?person GOT:col-got-lname ?LName .
    ?person GOT:col-got-birthdate ?BirthDate .
	FILTER (?BirthDate < "0280-01-01"^^xsd:date)
}
```

결과는 아래와 같다.

```
FName       LName       BirthDate
----------------------------
Tyrion      Lannister   0273-04-01
Jaime       Lannister   0266-02-21
Visserys    Targaryen   0276-05-05
Cersei      Lannister   0266-02-21
Rhaegar     Targaryen   0259-09-12
```

`PREFIX`를 추가했다.

```
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
```

여기서 이에 대해 설명하지 않고 넘어간다. SPARQL 쿼리에서 날짜를 필터링하거나 다른 방법으로 조작하려면 이 정보가 필요하다는 것만 알고 있도록 한다.

이 쿼리는 280년 1월 1일 이전에 태어난 모든 인물을 반환한다. 원한다면, `?Age`순으로 정렬한 것과 같은 방법으로 `?BirthDate` 순으로 정렬해 본다. 

## 유용성
다음과 같은 등록된 유권자 데이터세트가 있다고 가정해 본다.

- 생년월일
- 당적

SPARQL의 그룹화, 집계 기능과 함께 필터를 사용하면 특정 기간 내에 태어난 사람들의 정치적 성향을 분석할 수 있다.

## 연습
1) 17세 이하의 데이터세트에 있는 인물의 이름과 성을 반환하기 위해 SPARQL 쿼리를 작성해 본다. 또한 연령별로 결과를 나이 순으로 정렬한다.

```
PREFIX GOT: <https://tutorial.linked.data.world/d/sparqltutorial/>

```

2) 데이터세트에 있는 265년 이후부터 285년 이전에 태어난 인물의 이름과 성을 반환하는 쿼리를 작성한다.

```
PREFIX GOT: <https://tutorial.linked.data.world/d/sparqltutorial/>
PREFIX xsd:<http://www.w3.org/2001/XMLSchema#>
```
