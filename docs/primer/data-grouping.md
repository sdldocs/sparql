# 데이터 그룹핑
이미 데이터세트에 대해 최대값, 합계 값 및 기타 이러한 조작을 수행하는 방법을 배웠다. 이제 이를 GROUP BY라는 SPARQL 함수와 결합하려고 한다.  GROUP BY는 데이터세트의 데이터를 집계하여 데이터의 일부 또는 부분집한들에서 MAX와 같은 기능을 쉽게 실행할 수 있도록 한다.

## 학습할 키워드
- GROUP BY - 분석을 위해 데이터를 그룹으로 분할
- MAX와 MIN과 같은 집계 함수를 GROUP BY와 결합하여 데이터에 대한 새로운 결과를 얻는 방법

## GROUP BY를 사용하는 집계
SPARQL의 GROUP BY 함수를 통해 SPARQL의 집계 함수를 데이터 세트의 다른 데이터와 연결할 수 있다. 이는 SPARQL의 가장 강력한 기능 중 하나이며, 데이터를 통해 새로운 정보를 발견하는 시나리오를 보인다.

[정렬 및 요약 통계](sorting-and-summary-statistics.md)의 연습에서 데이터세트 인물의 최대 연령을 찾는 방법을 배웠다.
```
SELECT (MAX(?age) as ?maxAge)
```

`maxAge`를 데이터세트의 모든 인물의 나이 최대값으로 바인딩한다.

이제 각 가문에 속한 인물의 최대 연령을 찾으려면 어떻게 해야 할까? SPARQL의 GROUP BY 함수를 사용하여 이를 수행할 수 있다.
```
PREFIX GOT: <https://tutorial.linked.data.world/d/sparqltutorial/>

SELECT ?house (MAX(?age) as ?maxAge)

WHERE {
	?person GOT:col-got-house ?house ;
        	GOT:col-got-age ?age .
}
GROUP BY ?house
```

결과는 아래와 같다.

```
house       maxAge  
--------------------
Lannister         34
Targaryen         16
Stark             17
```

## ";" ?
이 쿼리는 이전에 작성한 쿼리와 조금 다르다. 다른 properties (`GOT:col-got-house` 및 `GOT:col-got-age`)들이 동일한 리소스(`?person`)를 사용하는 경우 리소스 다시 쓰기를 건너뛸 수 있다. 새로운 문장은 세미콜론으로 끝내고 마지막 문장은 마침표로 끝낸다. 위에 문장을 정확히 다음과 같이 해석할 수 있다.

```
?person GOT:col-got-house ?house .
?person GOT:col-got-age ?age .
```

위의 쿼리 내에서 MAX 대신 [SPARQL의 집계 함수](../sorting-and-summary-statistics/#aggregations)를 사용할 수도 있다. 쿼리가 `?maxAge` 변수를 출력하는 것보다 더 관련이 있는 것으로 변경하는 것이 바람직하다.


## 유용성
음주 습관에 대한 설문 조사의 답변으로 구성된 데이터세트가 있다고 가정하자. 이 데이터세트는 다음을 포함한다.

- 우편번호
- 나이
- 주당 은부 횟수

SPARQL의 AVG 함수와 "Zip Code"에 GROUP BY를 사용하면 인구의 모든 우편 번호에 대해 소비된 평균 음주 횟수를 얻을 수 있다.

## 연습
데이터세트에서 각 가문(Lannister, Stark 등)의 평균 연령을 출력한다.

```
PREFIX GOT: <https://tutorial.linked.data.world/d/sparqltutorial/>

```
