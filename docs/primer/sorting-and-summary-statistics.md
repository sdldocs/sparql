# 정렬 및 요약 통계
이제 데이터를 잘 활용할 수 있도록 도와주는 몇 가지 키워드와 기능에 대해 알아보자. SPARQL에는 데이터를 정렬하고 부분집합을 구하는 방법이 내장되어 있다. 또한 특정 property의 최소값과 최대값뿐만 아니라 평균 및 합계와 같은 일반적인 요약통계를 구을 수 있다.

## 학습할 키워드
- ORDER BY - 데이터를 정렬
- LIMIT - 특정 갯수의 결과를 검색
- MIN - 최소값을 반환
- MAX - 최대값을 반환
- AVG - 평균을 반환
- SUM - 합계 값을 반환
- COUNT - 값이 갯수를 반환

## 데이터 정렬
데이터를 정렬하려면 WHERE 절 다음에 **ORDER BY** 절을 사용한다. 예를 들어, ID로 데이터세트의 인물을 보는 대신 이름의 알파벳 순으로 보려면 다음과 같은 쿼리를 수행한다.
```
PREFIX GOT: <https://tutorial.linked.data.world/d/sparqltutorial/>

SELECT ?FName ?LName

WHERE {
  ?person GOT:col-got-fname ?FName .
  ?person GOT:col-got-lname ?LName .
}
ORDER BY ?FName
  # Sorts data in alphabetical order by first name
```

결과는 아래와 같다.

```
FName       LName
-------------------
Arya        Stark
Bran        Stark
Cersei      Lannister
Daenerys    Targaryen
Jaime       Lannister
Jon         Snow
Rhaegar     Targaryen
Rickon      Stark
Robb        Stark
Sansa       Stark
Tyrion      Lannister
Viserys     Targaryen
```

"**ORDER BY**" 구문에 "**DESC**"를 추가하여 `ORDER BY DESC(?LName)`로 변경하여 그 결과를 구해보자. 
```
PREFIX GOT: <https://tutorial.linked.data.world/d/sparqltutorial/>

SELECT ?FName ?LName

WHERE {
  ?person GOT:col-got-fname ?FName .
  ?person GOT:col-got-lname ?LName .
}
ORDER BY DESC ?LName
  # Sorts data in alphabetical descending order by last name
```
결과는 아래와 같다.

```
FName       LName
-------------------
Cersei      Lannister
Tyrion      Lannister
Jaime       Lannister
Jon         Snow
Arya        Stark
Bran        Stark
Rickon      Stark
Robb        Stark
Sansa       Stark
Daenerys    Targaryen
Rhaegar     Targaryen
Viserys     Targaryen
```

다음 `ORDER BY ?LName ?FName`을 실행하면, 성과 이름을 기준으로 결과를 정렬한다.
```
PREFIX GOT: <https://tutorial.linked.data.world/d/sparqltutorial/>

SELECT ?FName ?LName

WHERE {
  ?person GOT:col-got-fname ?FName .
  ?person GOT:col-got-lname ?LName .
}
ORDER BY ?LName ?FName
  # Sorts data in alphabetical order by last name and first name
```

결과는 아래와 같다.

```
FName       LName
-------------------
Cersei      Lannister
Jaime       Lannister
Tyrion      Lannister
Jon         Snow
Arya        Stark
Bran        Stark
Rickon      Stark
Robb        Stark
Sansa       Stark
Daenerys    Targaryen
Rhaegar     Targaryen
Viserys     Targaryen
```

## 지정된 갯수의 결과 검색
쿼리의 끝에 **LIMIT** 키워드를 사용하여 지정한 갯수의 결과만을 반환시틸 수 있다. 예를 들어, 데이터를 파악하기 위해 처음 5개의 레코드만 보려고 한다.
```
PREFIX GOT: <https://tutorial.linked.data.world/d/sparqltutorial/>

SELECT ?ID ?FName ?LName

WHERE {
  ?person GOT:col-got-id ?ID .
  ?person GOT:col-got-fname ?FName .
  ?person GOT:col-got-lname ?LName .
}
LIMIT 5
```

결과는 아래와 같다.

```
ID  FName       LName
-----------------------
 1  Tyrion      Lannister
 2  Jaime       Lannister
11  Viserys     Targaryen
12  Daenerys    Targaryen
 3  Cersei      Lannister
```

> **Note**: <br>
> LIMIT 키워드는 데이터세트에 수천 개의 행이 있을 때 매우 유용할 수 있다. 또한 LIMIT을 사용하면 쿼리가 원하는 데이터를 반환하는지 여부를 테스트할 때 쿼리의 계산 시간을 줄일 수 있다. 100만개 이상의 행을 반환하는 쿼리를 디버깅하는 데 매우 오래 걸린다.

또한 "LIMIT 1"을 "ORDER BY..." 및 "ORDER BY DESC..."와 결합하여 데이터세트에서 최소값과 최대값을 얻을 수도 있다. 

## 최소, 최대 그리고 평균값
쿼리의 SELECT 절에 **MIN()** 또는 **MAX()** 함수를 사용하여 데이터의 최소값 또는 최대값을 각각 반환한다. MIN()과 MAX() 함수의 장점은 문자열뿐만 아니라 숫자 값에서도 적동할 수 있다는 것이다.

```
PREFIX GOT: <https://tutorial.linked.data.world/d/sparqltutorial/>

SELECT (MIN(?FName) AS ?firstName)
  # Stores the minimum value bound to the ?FName variable in the ?firstName variable - only displays this value

WHERE {
  ?person GOT:col-got-fname ?FName .
}
```

결과는 아래와 같다.

```
firstName       
---------------
Arya
```

위의 쿼리에서 SELECT 절에서 사용할 수 있는 몇 가지 새로운 구문을 소개했다. 키워드 **AS**는 선택 후 괄호 안의 왼쪽과 오른쪽으로 나누어 특정 값을 특정 변수로 설정할 수 있도록 한다. 예를 들면 아래와 같다.

```
SELECT (MIN(?FName) AS ?firstName)
```

변수 `?FName`의 MIN 값을 `firstName`라는 새 변수에 바인딩한다.

결과를 살펴보면, 결과의 열 헤더인 property FName이 "firstName"으로 바인딩된다. 이것은 MIN() 함수를 사용한 바인딩이 작동했음을 보인다. 진행하면서 MAX()도 사용해 보자.

MIN() 및 MAX()와 달리 **AVG()**는 숫자 값에만 유효하고 문자열에는 적용할 수 없다. 데이터세트에서 모든 연령의 평균을 찾으려면 다음 쿼리를 사용할 수 있다.

```
PREFIX GOT: <https://tutorial.linked.data.world/d/sparqltutorial/>

SELECT (AVG(?age) AS ?avgAge)
  # Stores the average of values bound to the ?age variablae in the ?avgAge variable - only displays this value

WHERE {
  ?person GOT:col-got-age ?age .
}
```

결과는 아래와 같다.

```
avgAge       
------------
    18.6667
```

## 집계 <a id="aggregations"></a>
**SUM()**과 **COUNT()**를 사용하여 값과 레코드별 데이터를 집계할 수 있다. 다음은 SUM()의 예이다.
```
PREFIX GOT: <https://tutorial.linked.data.world/d/sparqltutorial/>

SELECT (SUM(?age) AS ?totalYears)
  # Stores the total of values bound to the ?age variable in the ?avgAge variable - only displays this value

WHERE {
  ?person GOT:col-got-age ?age .
}
```

결과는 아래와 같다.

```
totalYears       
------------
        168
```

COUNT()는 데이터세트에 누락된 데이터가 있을 수 있으므로 AVG() 및 SUM()과 함께 사용하면 매우 유용할 수 있다. 데이터세트에 있는 인물 중 세 명은 유효한 Age 값을 갖지 않았으므로 COUNT()를 사용하면 다른 수치 즉 평균값를 구성하는 데이터 포인트의 수를 알 수 있다.

## 유용성
특정 우편번호 지역에 거주하는 좌파 유권자의 평균 연령을 알아내기 위한 간단한 쿼리를 작성하거나 NBA 선수 중 가장 키가 큰 10명의 키를 찾는 것을 생각해자. 정렬과 집계는 이를 가능하게 한다.

## 연습

1) 데이터세트의 인물 중 가장 어린 연령을 출력하는 SPARQL 쿼리를 작성하라.
```
PREFIX GOT: <https://tutorial.linked.data.world/d/sparqltutorial/>

```

2) 데이터세트에 있는 인물 중 가장 높은 연령을 출력하는 SPARQL 쿼리를 작성하라.
```
PREFIX GOT: <https://tutorial.linked.data.world/d/sparqltutorial/>

```
