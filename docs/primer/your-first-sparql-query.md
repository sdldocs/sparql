# 첫 SPARQL 쿼리
[입문서 소개](../overview/#introduction)에서 언급한 바와 같이, 이 입문서는 가능한 한 빨리 SPARQL을 사용할 수 있도록 하기 위해 고안되었지만, 첫 번째 쿼리로 넘어가기 위해 이해해야 할 몇 가지 기본 개념을 알아야 한다.

## Query 대상인 데이터

"[GOT.csv](https://data.world/tutorial/sparqltutorial/file/GOT.csv)" 파일의 데이터세트을 사용한다. 이는 조지 R. R. 마틴의 *'왕좌의 게임 - 얼음과 불의 노래'* 시리즈에 나오는 12명의 중요 인물에 대한 생체 정보를 상세히 기록한 작은 데이터세트이다. 각 사람은 이름, 성, 집, 나이, 고유 ID 값과 같은 다양한 속성을 가지고 있다. Excel 또는 SQL에 익숙하다면, 친숙함을 느낄 수 있다. 그러나 SQL을 사용하여 쿼리하지 않고 "Linked Data(링크된 데이터)"를 검색할 수 있도록 설계된 언어인 SPARQL을 사용한다. 링크된 데이터란 정확히 무엇인가?

## 표 vs. 링크된 데이터

왕좌의 게임 데이터세트(GOT.csv)에 저장된 데이터는 일반적인 표 형식의 데이터일 뿐이다. CSV, Excel 워크북 또는 관계형 데이터베이스 등에서는 논리적으로 대부분의 데이터를 표 형식으로 저장하고 있다. 표 형식의 데이터 모델은 항목 목록과 관련 속성을 개별 파일에 효율적으로 저장하는 데 탁월하지만, 여러 데이터 세트에서 복잡한 통찰력을 한 번에 수집하기 위해 다른 데이터에 연결하는 기능에는 한계가 있다. 링크된 데이터 모델은 이를 개선하기 위해 설계되었으며, 이것이 모든 데이터를 저장하는 목적이기 때문이다. 표 형식으로 저장된 데이터를 링크된 데이터로 변환할 수 있다. 나중에 데이터셋을 함께 연결 튜토리얼에서 어떻게 작동하는지 알아보겠지만, 지금은 이 데이터를 이용한다.

## 첫 쿼리

### 학습할 키워드와 개념

- PREFIX - 데이터의 네임스페이스를 나타내는 약자를 지정
- SELECT - 쿼리 검색 결과를 화면에 출력할 데이터에 대한 변수를 지정
- WHERE - 데이터세트에서 추출할 데이터를 지정
- Variable - SELECT 또는 WHERE 섹션 내에서 향후 사용을 위해 데이터 값을 저장
- 링크된 데이터 모델의 기본 사항 및 표 형식의 데이터를 링크된 데이터로 변환하는 방법

`namespace`에 익숙하지 않아 PREFIX 정의가 불투명해 보일 수 있다. 현재로서는 PREFIX를 SPARQL 프로세서가 데이터를 찾기 위한 가이드로 생각하거나 프로그래머라면 `include` 또는 `import`로 생각할 수 있다.

다른 정의도 헷갈릴 수 있으므로 아래 각 키워드와 개념에 대해 자세히 설명하겠지만, 첫 번째 쿼리와 실행 방법을 살펴보는 것으로 시작하자.

### 퀴리 실행
첫 번째 SPARQL 쿼리이다. SPARQL 쿼리의 기번적 구성을 보이기 위해 아래 예를 준비하였다. 이 간단한 쿼리는 모든 사용자의 ID 및 FName 값을 테이블 형식으로 출력한다.

```sparql
PREFIX GOT: <https://tutorial.linked.data.world/d/sparqltutorial/>

SELECT ?ID ?FName

WHERE {
    ?person GOT:col-got-id ?ID .
    ?person GOT:col-got-fname ?FName .
}
```

쿼리를 실행하면 다음과 결과를 출력한다.
```
ID  FNname
-----------
 1  Tyrion
 2  Jaime
11  Viserys
12  Daenerys
 3  Cersei
 4  Robb
 5  Jon
 6  Sansa
 7  Arys
 8  Bran
 9  Rickon
10  Rhaegar
```

쿼리 단계를 자세히 살펴보기 전에 링크된 데이터를 저장한 방법에 대해 간단히 설명한다.

### 데이타에 대한 심층 분석
표 형식의 데이터세트를 링크된 데이터 형식(RDF 형식)으로 전환하여 저장된다. 즉 RDF 형식의 문장 집합으로 변환되어 저장한다. 각 문장은 세 부분으로 구성됩니다.

1. 리소스(resource),
2. 속성 이름(property name)과
3. 속성 값(property value)

데이터 테이블의 각 행을 리소스로, 열 헤더를 속성 이름으로, 데이터 셀을 속성 값으로 생각할 수 있다. 예를 들어, *Game of Thrones* 데이터를 구성하는 한 문장은 "4행의 *리소스*는 *속성 이름* 'House'와 해당 *속성 값* 'Stark'를 가지고 있다."를 나타낸다. 다음 세 그림은 이 문을 표 형식, RDF 그래프와 일반 영어 문장으로 표현한 것이다. 세 도표는 모두 같은 것을 표현한 것이다.

![fig-1](../images/fig-primer-01.png)

표 형식의 그림에서 4행의 파란색 리소스와 빨간색 속성 이름이 겹친 것이 보라색 속성 값이다.

![fig-2](../images/fig-primer-02.png)

RDF 데이터(또는 "그래프") 뷰에서 빨간색 속성 이름은 파란색 리소스를 해당 보라색 속성 값에 연결한다.

![fig-3](../images/fig-primer-03.png)

이를 영어문장으로 표현하면, 데이터는 파란색 제목, 빨간색 술어 및 보라색 객체가 있는 3부분으로 표현할 수 있다. 실제로 "제목, 술어, 객체 3 트리플"로 설명되는 RDF 데이터를 자주 볼 수 있다.

> **In-depth**: <br>
> Rob Stark의 데이터가 RDF 데이터 파일에 어쩧게 저장되는 지 자세히 살펴보자. 참고로 비슷해 보일 수 있지만 SPARQL 쿼리는 아니며 `Turtle`(일명 `.ttl`)이라는 RDF 데이터 구문 형식의 목(mock) 데이터 파일이다.
> 
> 영어와 마찬가지로 데이터 문은 마침표로 끝난다. 데이터 값은 따옴표 " "로 표시되며 주소(또는 "자원 식별자(Resource Identifier")는 "<\>"로 표시된다. 접두사 "GOT:"는 SPARQL 프로세서에 데이터 저장 위치를 알려주는 데 필요하다.
```
# Robb Stark's info
# BTW, you can make comments in RDF/SPARQL with the '#' symbol

@prefix GOT: <https://tutorial.linked.data.world/d/sparqltutorial/>

GOT:row-got-3 GOT:col-got-id "4" .
GOT:row-got-3 GOT:col-got-fname "Robb" .
GOT:row-got-3 GOT:col-got-lname "Stark" .
GOT:row-got-3 GOT:col-got-house "Stark" .

# Note: The resource URI ends in '3' because of 0 indexing, i.e. the first resource is the "0th" resource.
```

이제 데이터가 어떻게 저장되는지 알았으므로 SPARQL 쿼리를 한 줄씩 살펴보자.

### 첫번빼 줄 -  PREFIX
```
PREFIX GOT: <https://tutorial.linked.data.world/d/sparqltutorial/>
```

이 행은 `https://tutorial.linked.data.world/d/sparqltutorial/`을 참조하도록 단축 접두사 `GOT:`를 설정한다 (프로그래밍 경험이 있는 경우 문자열을 참조하는 변수를 선언하는 것으로 생각할 수 있다). 이는 URL처럼 보일 수 있지만, 실제로는 URI, 즉 **U**niform **R**esource **I**dentifier (URL은 URI의 한 종류이다). URI는 웹 페이지를 참조하는 URL과 달리 리소스 또는 해당 리소스에 대한 정보를 참조한다. URI의 목적은 데이터에 고유한 이름을 부여하는 위한 것이다.

기능적 목적 외에도 접두사는 스타일의 목적도 제공한다. 접두사를 설정하지 않으면 리소스의 속성 식별자가 길어지고 관리하기가 어려워질 수 있다. Prefix를 설정할 필요는 없지만 SPARQL 쿼리를 읽기 쉽고 쿼리 작성을 훨씬 쉽게 만들 수 있다. 그렇지 않으면, 아래처럼 간단한 구문이

```
GOT:row-got-3 GOT:col-got-fname "Robb".
```

다음처럼 길고 복잡하여 질 수 있다.

```
<https://tutorial.linked.data.world/d/sparqltutorial/row-got-3> <https://tutorial.linked.data.world/d/sparqltutorial/col-got-fname> "Rob".
```

> **In-depth**: <br>
> PREFIX를 설정하지 않은 첫 번째 쿼리는 아래와 같다.

```
SELECT ?ID ?FName

WHERE
{
    ?person <https://tutorial.linked.data.world/d/sparqltutorial/col-got-id> ?ID .
    ?person <https://tutorial.linked.data.world/d/sparqltutorial/col-got-fname> ?FName .
}
```

이것은 기술적으로 100% 정확한 구문이지만, PREFIX 선언이 없으면 읽기 어렵고 사용자 친화적이지 않다.

### 다음 줄 - SELECT
```
SELECT ?ID ?FName
```

SQL을 사용해 본 경험이 있다면 이 줄은 친숙할 것이다. SPARQL 프로세서에 표시할 데이터(이 경우 ID 및 First Name 속성 값)만 알려준다.

왜 물음표가 있을까?

SPARQL에서 물음표는 변수를 나타내며, 이는 일반 컴퓨터 프로그래밍 언어에서 변수와 같은 방식으로 동작한다. 변수는 미래에 사용할 값을 가지고 있다. `SELECT ?ID ?Fname`은 기본적으로 다음과 같은 의미이다. "`?ID`와 `?FName`에 저장된 값을 찾아 해당 값을 출력한다."

> **Note**: <br>
> '?'가 있으면 변수로 생각한다. 변수는 일반적으로 SPARQL에서 물음표로 시작하지만 드물게 '$'로 표시될 수도 있다. 일반적인 관습은 물음표를 사용하는 것이다.

### 끝으로 - WHERE 절
```
WHERE
{
    ?person GOT:col-got-id ?ID .
    ?person GOT:col-got-fname ?FName .
}
```

WHERE 절은 프로세서에 선택 중인 변수에 어떤 속성 값을 채우고 데이터를 선택하는 방법을 지정한다. SPARQL 프로세서는 WHERE 절에서 선언한 패턴을 저장된 문장의 세 부분과 일치시킵니다. 위 절을 자세히 보았다면, 질의문이 예제 데이터 파일에 있는 Robb의 문장과 매우 유사하다는 것을 눈치챘을 것이다. SPARQL 쿼리의 WHERE 절에 있는 문장은 RDF와 동일한 구조를 갖는다.

```
{ resource propertyName propertyValue . }
```

위의 쿼리에서는 `?ID`와 `?FName` 변수에 값을 지정하기 위하여 두 문장을 사용한다. 두 문장 모두 다른 변수인 `?person`으로 시작하지만 `?person`은 SELECT 절에 없어 화면에 출력되지 않으며 데이터를 연결하는 WHERE 절 내에서만 사용된다. SPARQL에서 변수는 SPARQL 프로세서에게 문장 일부분이 쿼리와 일치하고 쿼리의 다른 절에서 사용할 수 있도록 변수에 저장된 값이 있는 문장을 알려준다. 

즉, SPARQL 프로세서는 WHERE 섹션에 정의된 패턴과 일치하는 (RDF로 저장된) 데이터 문장들을 검색하여 해당 문장의 데이터를 변수에 바인딩한다. 그결과 위의 WHERE 절의 첫 번째 줄은 "ID가 있는 사람들을 찾아라"이고 두 번째 줄은 "그리고 그 사람들 각각에 대해 FName이 있으면 출력하여라"라는 의미이다. WHERE 절의 각 문장 사이에는 암시적 AND가 있다고 해석한다. 즉, 이 쿼리는 모든 문장들과 일치하는 패턴만을 반환한다. 이 문장들의 순서는 중요하지 않다. 문장들의 순서를 바꾸어도 여전히 쿼리 프로세서가 데이터에 대해 일치하도록 동일한 패턴을 구성한다. 아래 그림은 패턴 매칭 과정을 묘사하고 있으며 자세히 설명한다.

> **Note**: <br>
> 특히 다른 데이터세트와 열결할 때 SPARQL 프로세서가 정확한 데이터를 찾을 수 있도록 리소스와 property 이름은 특정 namespace에 속해야 합니다. 데이터의 세 절의 문장 중에 있는 URI는 쿼리의 SELECT 절에 의해 선택되지 않을 수도 있지만, 이러한 URI는 검색할 데이터와 데이터를 연결하기 위해 서로 다른 데이터를 상호 참조하는 방법을 식별하는 데 필요하다.

![fig-4](../images/fig-primer-04.png)

> **In-depth**: <br>
> 다음은 WHERE 절의 단계별 작동 방식이다.
> 
> 1. 첫 번째 문장 "`?person GOT: col-got-id ?ID.`"는 SPARQL 프로세서에 데이터를 살펴보고 ID property를 갖는 리소스의 해당 값을 `?ID`에 저장한다. 이 경우 Tyrion을 참조하는 리소스를 찾을 수 있다. Tyrion이 데이터세트에 제일 먼저 나열되어 있지만 항상 그런 것은 아니다. SPARQL 프로세서는 어떤 순서로든 데이터를 찾을 수 있기 때문이다. 여기서는, `?ID` 값으로 "1"을 갖는다.
> 2. 다음 SPARQL 프로세서는 쿼리 문의 나머지 부분에 대해 해당 리소스(Tyrion의 행 - `GOT:row-got-0`)를 `?person`에 바인딩한다.
> 3. 다음 문장은 "`?person GOT:col-got-fname ?FName`"은 SPARQL 프로세서에 `?person`에 저장된 리소스에 `GOT:col-got-fname`라는 property가 있는지 확인하고, 있다면 해당 값을 `?FNAME`에 할당한다. 첫 번째 행에 FName이 있어서 `?FName`에 "Tyrion"을 저장한다.
> 4. 이제 모든 쿼리 문이 실행되면, SPARQL 프로세서는 데이터의 모든 리소스에 대해 쿼리 문을 반복 수행하여 ?에 모든 ID와 FName 값을 `?ID`와 `?FName` 변수에 저장한다. 두 property에 모두 데이터가 있는 모든 리소스에 대해 이 작업을 수행한다.
>
---
> **BTW**: <br>
> 이 경우 모든 리소스(또는 데이터 행)는 책에 나오는 사람을 나타내므로 변수 이름을 `?person`으로 하였다. 프로그래밍에서와 같이 변수의 목적을 나타내는 방식으로 변수 이름을 지정하는 것이 바람직하다.
---
> **BTW**: <br>
> 리소스에 ID나 FName이 없으면 어떻게 될까? 간단히 말해서, 자원은 전혀 표시되지 않는다. 이 예에서는 데이터의 모든 리소스에 ID와 FName이 모두 존재하므로 조회가 *모든* 리소스에 대한 적절한 정보를 찾아 반환하고 출력한다. 이 쿼리의 WHERE 절은 ID와 FName을 *모두* 가진 리소스를 요청한다.
---
> **BTW**: <br>
> SPARQL은 공백을 인식하지 않으므로 WHERE 절을 다음과 같이 한 줄로 작성할 수 있다. 
> 
> ```
> WHERE { ?person GOT:col-got-id ?ID . ?person GOT:col-got-fname ?FName . }
> ```

첫 SPARQL 쿼리는 여기까지.

## 요약
- 연결된 데이터의 기본 및 데이터의 RDF로 변한
- 변수는 무엇이며, SPARQL 쿼리에 원하는 값을 저장하는 방법
- SPARQL 쿼리의 세 기본 키워드 (PREFIX, SELECT 및 WHERE)와 이를 사용하여 간단한 쿼리 작성

## 연습
PREFIX, SELECT 및 WHERE를 사용하여 GOT.csv를 RDF로 변환한 데이터세트에 있는 모든 인물의 이름과 성을 출력하는 SPARQL 쿼리를 작성하여라.

```
PREFIX GOT: <https://tutorial.linked.data.world/d/sparqltutorial/>

```