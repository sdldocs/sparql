# 데이터 세트 연결

## 학습할 내용

-  하나의 쿼리 내에서 여러 데이터셋에 액세스하는 방법

## 데이터 가져오기
이제 몇 가지 데이터를 연결하도록 하여 보자. 데이터 세트가 두 개 GOT.csv와 GOT2.csv가 있다. GOT.csv에는 이름, 나이, 집, 생년월일 칼럼이 있으며, GOT2.csv에는 각 인물의 관점(POV)에 따른 각 책에 나타난 페이지 수가 나열되어 있다.

SPARQL 쿼리의 힘을 사용하여 하나의 쿼리 내에서 이 두 데이터세트 모두에서 데이터를 쿼리하고 조작할 수 있다. 다음은 예이다.

```
PREFIX GOT: <https://tutorial.linked.data.world/d/sparqltutorial/>

SELECT ?FName ?age ?book1PagesPOV

WHERE {
    ?person GOT:col-got-fname ?FName .
    ?person2 GOT:col-got2-name ?FName .
    ?person GOT:col-got-age ?age .
    ?person2 GOT:col-got2-book1pagespov ?book1PagesPOV .
}
```

결과는 아래와 같다.

```
FName       Age     book1PagesPOV
---------------------------------
Tyrion       27          87
Jaime        34           8
Daenerys     16         117
Cersei       34           0
Jon          17          83
Sansa        14          54
Arya         11          47
Bran         10          60
Rickon        5           0
```

실행 결과에는 각 인물의 POV에서 가져온 1권의 페이지 수와 나이가 포함되었다.

GOT.csv의 이름이 GOT2.csv의 이름과 일치하므로 데이터가 서로 연결되기 때문이다. `?person`과 `?person2`를 사용하여 두 데이터세트에서 항목을 가져온다. `?person` 리소스는 첫 번째 데이터세트에 해당하고 `?person2` 리소스는 두 번째 데이터세트에 해당된다.

이제 흥미로운 일들을 시작할 수 있다. 다음은 나이 항목이 있는 인물을 예로 들어 책 1에 있는 POV가 전체 POV 페이지 중 몇 퍼센트가 인가를 알아보자.

```
PREFIX GOT: <https://tutorial.linked.data.world/d/sparqltutorial/>

SELECT ?FName ?age (?book1PagesPOV / ?totalPagesPOV * 100 AS ?book1Percent)

WHERE {
    ?person GOT:col-got-fname ?FName .
    ?person2 GOT:col-got2-name ?FName .
    ?person GOT:col-got-age ?age .
    ?person2 GOT:col-got2-book1pagespov ?book1PagesPOV .
    ?person2 GOT:col-got2-totalpagespov ?totalPagesPOV .
}
ORDER BY DESC(?age)
```

결과는 아래와 같다.

```
FName       Age     book1PagesPOV
---------------------------------
Cersei       34          0.0
Jaime        34          0.0
Tyrion       27         16.5714
Jon          17         18.9498
Daenerys     16         29.9233
Sansa        14         23.1760
Arya         11         14.3293
Bran         10         24.7934
Rickon        5     No Data
```

## 유용성
우편 번호별로 분류된 정치적 성향에 대한 데이터를 가지고 있다 하자. 우편 번호별로 그룹화된 인구 조사 데이터를 찾을 수 있다면, 이 두 데이터세트를 연결하여 인구 통계와 정치적 성향 사이의 흥미로운 상관관계를 우편 번호별로 찾을 수 있는지 확인할 수 있다.

## 연습
SPARQL 쿼리를 작성하여 "Stark" 가문의 모든 인물에 대한 총 POV 페이지를 반환하여라.

```
PREFIX GOT: <https://tutorial.linked.data.world/d/sparqltutorial/>

```
