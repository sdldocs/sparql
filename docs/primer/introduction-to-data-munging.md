# 데이터 Munging 개요
데이터세트을 수정하여 새 결과를 생성하고자 하는 경우 변수를 생성하고 값을 지정할 수 있습니다. 어떻게 바인딩된 값을 가진 변수를 필터링할 수 있을까?

## 학습할 키워드

- BIND - 값을 변수에 바인딩
- 바인딩된 변수 필터링
- MONTH - 날짜 값에서 월을 반환
- CONCAT - 문자열을 연결
- STRLEN - 문자열의 길이를 반환

## BIND
다음과 같이 SPARQL의 BIND 함수를 사용하면 변수에 값을 할당할 수 있다.

```
BIND (MONTH(?birthDate) AS ?birthMonth)
```

왼쪽 `[MONTH(?birthDate)]`는 변수의 값을 설정하는 곳이고 오른쪽 `[?birthMonth]`는 변수를 정의한다. 키워드 `AS`는 양쪽을 구분한다. 

이 경우 SPARQL 함수 MONTH를 사용하여 각 인물의 생년월일에서 월을 캡처한 다음 이 값을 `?birthMonth`에 할당한다. 전체 SPARQL 쿼리는 다음과 같다.

```
PREFIX GOT: <https://tutorial.linked.data.world/d/sparqltutorial/>

SELECT ?FName ?LName ?birthMonth

WHERE {
	?person GOT:col-got-fname ?FName ;
        	GOT:col-got-lname ?LName ;
        	GOT:col-got-birthdate ?birthDate .
	BIND (MONTH(?birthDate) AS ?birthMonth)
}
```

결과는 아래와 같다.

```
FName       LName       birthMonth
----------------------------------
Tyrion      Lannister           4
Jaime       Lannister           2
Viserys     Targaryen           4
Daenerys    Targaryen           6
Cersei      Lannister           2
Robb        Stark              10
Jon         Snow               12
Sansa       Stark               2
Arya        Stark               4
Bran        Stark               7
Rickon      Stark               1
Rhaegar     Targaryen           9
```

데이터세트에 있는 인물의 탄생월 표를 반환한다.


## FILTER와 BIND
왜 BIND가 유용한가? 이제 SPARQL의 필터 기능을 `?birthMonth`에 실행할 수 있기 때문이다. 2월에 태어난 인물를 선별해 보자.

```
PREFIX GOT: <https://tutorial.linked.data.world/d/sparqltutorial/>

SELECT ?FName ?LName ?birthMonth

WHERE {
	?person GOT:col-got-fname ?FName ;
        	GOT:col-got-lname ?LName ;
        	GOT:col-got-birthdate ?birthDate .
	BIND (MONTH(?birthDate) AS ?birthMonth)
	FILTER(?birthMonth = 2)
}
```

결과는 아래와 같다.

```
FName       LName       birthMonth
----------------------------------
Jaime       Lannister           2
Cersei      Lannister           2
Sansa       Stark               2
```

깔끔하죠?

## 필터 및 BIND와 함께 SPARQL 필터 및 집계 함수 사용
SPARQL의 문자열 연결 함수 CONCAT와 함께 사용해 보겠다. 

```
BIND (CONCAT(?FName, " ", ?LName) AS ?name)
```

이 한 줄로 이름, 공백 및 성을 연결하고, 변수 `?name`에 할당되었다. 그래서 ["Tyrion", "", "Lannister"]는 ["Tyrion Lannister"]가 된다.

이제 전체 이름이 12자 보다 긴 것에 대해 필터링해 보자. 문자열의 길이를 반환하는 SPARQL의 STRLEN 함수를 사용하여 이를 수행할 수 있다.

```
PREFIX GOT: <https://tutorial.linked.data.world/d/sparqltutorial/>

SELECT ?name

WHERE {
	?person GOT:col-got-fname ?FName ;
        	GOT:col-got-lname ?LName ;
	BIND (CONCAT(?FName, " ", ?LName) AS ?name)
    FILTER (STRLEN(?name) > 12)
}
```

결과는 아래와 같다.

```
    name
------------------
Tyrion Lannister
Jaime Lannister
Viserys Targaryen
Daenerys Targaryen
Cersei Lannister
Rhaegar Targaryen
```


## 유용성
축구선수들의 생년월일이 포함된 데이터세트가 있다. FILTER, BIND 및 SPARQL의 MONTH 함수를 사용하여 플레이어가 태어난 달 데이터를 생성할 수 있다. 또한 SPARQL의 COUNT 및 SUM 함수를 추가하면 축구 선수의 생년에 대한 흥미로운 상관 관계나 추세가 있는지 확인할 수 있다.

## 연습
4월에 태어난 캐릭터의 (하나의 변수로) 전체 이름을 반환하는 SPARQL 쿼리를 작성하라.

```
PREFIX GOT: <https://tutorial.linked.data.world/d/sparqltutorial/>

```
