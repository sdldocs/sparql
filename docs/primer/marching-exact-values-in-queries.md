# Matching Exact Values in 쿼리
[지난 페이지](your-first-sparql-query.md)에서는 변수를 사용하여 한 번에 많은 데이터를 출력하는 방법에 대해 배웠다. 이 접근 방식은 예를 들어 모든 리소스의 이름과 성을 표시하기 위해 데이터세트를 처음 탐색할 때 사용할 수 있다.

그러나 특정 값을 포함하는 데이터의 특정 부분 집합 또는 항목을 찾으려면 어떻게 해야 하나? 

## 학습할 KEYWORD와 개념
- Literals - 이중 따옴표("", "foo", "bar", "SPARQL" 등) 안에 0자 이상의 문자열
- OPTIONAL - 데이터세트에 없을 수 있는 데이터를 검색

## 문자열 검색

### Literals
아래 쿼리는 "literal" 형태의 새로운 구문을 포함하고 있으며, 이는 "따옴표 안에 정확한 단어 또는 구"를 표현한다 . 데이터세트에 "Daenerys"라는 이름을 가진 사람이 있고 그녀의 생체 정보를 얻으려고 한다.

```
PREFIX GOT: <https://tutorial.linked.data.world/d/sparqltutorial/>

SELECT ?FName ?LName ?House ?Age

WHERE
{
    ?person GOT:col-got-fname "Daenerys" .
    ?person GOT:col-got-fname ?FName .
    ?person GOT:col-got-lname ?LName .
    ?person GOT:col-got-house ?House .
    ?person GOT:col-got-age ?Age .
}
```

결과는 아래와 같다.

```
FName       LName       House       Age
----------------------------------------
Daenerys    Targaryen   Targaryen    16
```

이 쿼리는 먼저 FName 값이 "Daenerys"인 리소스를 검색한다. 이 경우 "Daenerys"는 문자열이며 SPARQL 프로세서가 인식할 수 있도록 리터럴 형식인 따옴표로 표시되어야 한다. 프로세서가 리소스를 찾으면 나머지 문장은 적절한 값을 `?FName`, `?LName`, `?House` 및 `?Age` 변수에 바인딩한다.

`?person GOT:col-got-fname`으로 시작하는 두 줄이 필요했다. 하나는 literal의 "Daenerys"이고 다른 하나는`?FName` 변수를 위한 것이다. 왜 그럴까? `?person GOT:col-got-fname ?FName` 줄을 제거하고 실행하여 보면 다음과 같은 결과를 얻는다.

```
Nname      LName       House       Age
----------------------------------------
No data    Targaryen   Targaryen    16
```

이 쿼리는 SPARQL 프로세서에 FName에 "Daenerys"가 있는 리소스를 찾도록 지시했을 수 있으나 `?FName` 변수를 SELECT했을 수도 있다. 단, 값을 `?FName` 변수에 명시적으로 바인딩하지 않으면 쿼리는 해당 property에 대해 아무것도 반환하지 않는다.


## 숫자 값 탐색
문자열과 달리 정수와 같은 숫자 값은 SPARQL 프로세서가 이를 인식하기 위해 따옴표를 필요로 하지 않는다. 예를 들어, 34세의 모든 인물을 검색하려면 아래와 같이 질의를 작성한다.

```
PREFIX GOT: <https://tutorial.linked.data.world/d/sparqltutorial/>

SELECT ?FName ?LName ?House ?Age

WHERE
{
    ?person GOT:col-got-age 34 .
    ?person GOT:col-got-fname ?FName .
    ?person GOT:col-got-lname ?LName .
    ?person GOT:col-got-house ?House .
    ?person GOT:col-got-age ?Age .
}
```

결과는 아래와 같다.

```
FName       LName       House       Age
----------------------------------------
Jaime       Lammister   Lammister    34
Cersei      Lammister   Lammister    34
```

SPARQL는 dates와 booleans과 같은 더 많은 데이터 타입을 지원한다. 이 튜토리얼의 뒷부분에서 이에 설명할 예쩡이다.

## 결측피를 갖은 데이터 탐색
많은 데이터 세트에 결측값(missing value)이 있으며, 기본 SPARQL 쿼리는 WHERE 절의 패턴이 쿼리 중인 데이터 패턴과 정확히 일치하는 경우에만 데이터를 반환한다. 이는 SPARQL이 패턴 매칭 쿼리 언어이기 때문이다. 데이터는 WHERE 내에 선언된 문장의 각 부분에 유효한 데이터가 있는 경우에만 쿼리와 일치한다. 즉, 데이터 리소스에 누락된 속성 이름 또는 값이 WHERE에 요청된 경우 SPARQL이 아무것도 반환하지 않는다. 예를 들어 리소스의 ID, 이름 및 나이를 출력하는 쿼리를 수행하여 보자.

```
PREFIX GOT: <https://tutorial.linked.data.world/d/sparqltutorial/>

SELECT ?ID ?FName ?Age

WHERE {
  ?person GOT:col-got-id ?ID .
  ?person GOT:col-got-fname ?FName .
  ?person GOT:col-got-age ?Age .
}
```

결과는 아래와 같다.

```
ID  FName       Age
-------------------
 1  Tyrion       27
 2  Jaime        34
12  Daenerys     16
 3  Cersei       34
 5  Jon          17
 6  Sansa        14
 7  Arya         11
 8  Bran         10
 9  Rickon        5
```

보시다시피, 세 가지 property 이름에 대해 유효한 property 값을 가진 리소스만 나열되어 있다. 불행히도, 잘 알려진 인물 중 일부 Robb, Viserys와 Rhaegar는 유효한 연령 값을 가지고 있지 않았다.

WHERE 절의 OPTIONAL 키워드를 사용하여 데이터에서 일치시킬 선택적 패턴을 나타낼 수 있다. 즉, OPTIONAL을 사용하면 있을 수도 있고 없을 수도 있는 데이터를 검색할 수 있다! Robb, Viserys와 Rhaegar를 츨력하기 위하여 `?person GOT: col-got-age ?Age`를 `OPTIONAL {?person GOT:col-got-age ?Age} .`로 변경하여 보자. 

```
PREFIX GOT: <https://tutorial.linked.data.world/d/sparqltutorial/>

SELECT ?ID ?FName ?Age

WHERE {
  ?person GOT:col-got-id ?ID .
  ?person GOT:col-got-fname ?FName .
  OPTIONAL {?person GOT:col-got-age ?Age .}
}
```

결과는 아래와 같다.

```
ID  FName       Age
-------------------
 1  Tyrion       27
 2  Jaime        34
11  Viserys     No data
12  Daenerys     16
 3  Cersei       34
 4  Robb        No data
 5  Jon          17
 6  Sansa        14
 7  Arya         11
 8  Bran         10
 9  Rickon        5
10  Rhaegar     No data
```

## 유용성

이러한 쿼리를 실행하여 데이터를 빠르고 효과적으로 가져올 수 있다. 또한 데이터를 사용하여 이러한 쿼리로 SELECT한 데이터를 다운로드할 수 있다. 여러분이 모든 인구조사 데이터를 가지고 있고 캘리포니아에 살고 있고 학교 선생님인 20-30세 사이의 사람들에 대한 데이터만 얻고 싶다면 간단한 쿼리를 사용하면 전체 대용량 데이터 파일을 다운로드할 필요 없이 이 모든 데이터를 SELECT하여 즉시 다운로드할 수 있다.

## 연습
문자열 리터럴을 사용하여 데이터세트에서 "Stark"를 찾아 "Stark" 가문과 관련된 모든 인물을 출력하는 SPARQL 쿼리를 작성하여라.

```
PREFIX GOT: <https://tutorial.linked.data.world/d/sparqltutorial/>

```
